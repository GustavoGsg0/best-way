
# README #  
  
# Rota de Viagem

Um turista deseja viajar pelo mundo pagando o menor preço possível independentemente do número de conexões necessárias. Vamos construir um programa que facilite ao nosso turista, escolher a melhor rota para sua viagem. 
  
### Sumário ###  
  
 - Como executar a aplicação
 - Estrutura dos arquivos/pacotes
 - Explique as decisões de design adotadas para a solução
 - Descreva sua API Rest de forma simplificada
  
#### Como executar a aplicação ####
 1. Crie um diretório tmp(esse nome está fixo no código, para ficar mais simples) na raiz do seu disco C:\. Exemplo: C:\tmp 
 2. Navegue pelo prompt até a pasta onde se encontra o arquivo (bex-0.0.1-SNAPSHOT.jar). Exemplo: cd C:\git\best-way
 3. Agora digite o comando **"java -jar bex-0.0.1-SNAPSHOT.jar input-routes.csv"**, sendo input-routes.csv o nome do arquivo que deve conter as entradas iniciais do programa no diretório do passo 2.
 4. No prompt aparecerá uma pergunta *"Para prompt digite (P), para rest digite (R): "*, sendo que se digitar "R" a aplicação iniciará com o spring boot e será possível o teste via Postamn com a collection que eu deixei no /resources/collection_postman e se digitar "P" a aplicação segue no promp com a opção de fazer a consulta ali mesmo.
  
### Estrutura dos arquivos/pacotes ###  
  
* Entrypoint - Onde contém os endpoints 
* Gateway - Onde contém a interface que separa a aplicação do obtentor de dados, facilitando a mudança para um banco de dados por exemplo.
* Model - Separei os modelos de entrada e saída (Request, Response) além do modelo interno dos dados
* Provider - Provedor dos dados, onde contém a implementação da leitura e persistência dos dados no arquivo
* Services - Onde contém os serviços que se conectam aos endpoints
* Utils - Pacote onde contém classes utilitárias
  
### Explique as decisões de design adotadas para a solução ###  
Está desenvolvido no padrão clean architecture de camadas.
Onde cada camada pode ser trocada e testada com independência.

### Descreva sua API Rest de forma simplificada ### 

 1. POST - Insere Roteiros - Essa Api recebe uma lista de entradas e persiste no seu arquivo localizado no C:\tmp\input-routes.csv. Exemplo na collection dentro do projeto.
 2. GET - Consulta Roteiro Mais Barato - Essa Api recebe um parâmetro "roteiro" no formato "GRU-ORL" e faz a construção do roteiro mais barato e devolve no formado "GRU - CDG - SCL - ORL > $35"
