package com.desafio.bex.useCases;

import com.desafio.bex.gateway.RoteirosGateway;
import com.desafio.bex.model.DadosRoteiroRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoteirosUseCase {

    @Autowired
    private RoteirosGateway gateway;

    public String consultarMelhorRota(final String roteiro) {
        return gateway.consultarMelhorRota(roteiro);
    }

    public void insereRoteiros(final DadosRoteiroRequest roteiros) {
        gateway.insereRoteiros(roteiros);
    }
}
