package com.desafio.bex.gateway;

import com.desafio.bex.model.DadosRoteiro;
import com.desafio.bex.model.DadosRoteiroRequest;

import java.io.IOException;
import java.util.List;

public interface RoteirosGateway {

    List<DadosRoteiro> darCargaDados() throws IOException;
    List<DadosRoteiro> darCargaDados(String nomeArquivo);
    void insereRoteiros(DadosRoteiroRequest roteiros);
    String consultarMelhorRota(String roteiro);
}
