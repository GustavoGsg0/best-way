package com.desafio.bex;

import com.desafio.bex.provider.RoteirosProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class BexApplication {

	public static void main(String[] args) {
		RoteirosProvider provider = new RoteirosProvider();
		provider.darCargaDados(args[0]);

		Scanner s = new Scanner(System.in);
		String resposta = null;
		boolean isEscolhido= false;

		System.out.println("Para prompt digite (P), para rest digite (R): ");
		String res = s.next();

		if("P".equals(res)){
			do{

				System.out.println("please enter the route: ");
				String rotaSolicitada = s.next();
				String melhorRota = provider.consultarMelhorRota(rotaSolicitada);
				System.out.println("best route: " + melhorRota);

				System.out.println("Continua busca? S/N ");
				resposta = s.next();
			}while("S".equals(resposta));
		}else{
			SpringApplication.run(BexApplication.class, args);
		}

	}

}
