package com.desafio.bex.provider;

import com.desafio.bex.gateway.RoteirosGateway;
import com.desafio.bex.model.DadosRoteiro;
import com.desafio.bex.model.DadosRoteiroRequest;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class RoteirosProvider implements RoteirosGateway {

    public static final String DEFAULT_SEPARADOR = ",";

    @Override
    public List<DadosRoteiro> darCargaDados() {
        return darCargaDados("input-routes.csv");
    }

    @Override
    public List<DadosRoteiro> darCargaDados(final String nomeArquivo) {
        List<DadosRoteiro> listDadosRoteiros = new LinkedList<>();

        try{
            BufferedReader reader = getBufferedReader(nomeArquivo);

            for (String line; (line = reader.readLine()) != null;) {
                String arr[] = line.split(DEFAULT_SEPARADOR);
                listDadosRoteiros.add(buildDadosRoteiro(arr));
            }

            reader.close();
        }catch(Exception e){
            e.printStackTrace();
        }

        return listDadosRoteiros;
    }

    @Override
    public void insereRoteiros(final DadosRoteiroRequest roteiros) {
        try{
            BufferedWriter csvWriter = getBufferedWriter("input-routes.csv");

            roteiros.getRoteiros().forEach(d -> {
                StringBuilder str = new StringBuilder();
                str.append(d.getOrigem());
                str.append(DEFAULT_SEPARADOR);
                str.append(d.getDestino());
                str.append(DEFAULT_SEPARADOR);
                str.append(d.getValor());
                try {
                    csvWriter.append("\n");
                    csvWriter.append(str);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            csvWriter.flush();
            csvWriter.close();
        }catch(Exception e){
            e.printStackTrace();
        }

        darCargaDados();
    }

    private static BufferedWriter getBufferedWriter(final String nomeArquivo) throws IOException {
        FileWriter csvWriter = new FileWriter("C:\\tmp\\" + nomeArquivo,true);
        BufferedWriter writer = new BufferedWriter(csvWriter);
        return writer;
    }

    private static BufferedReader getBufferedReader(final String nomeArquivo) throws IOException {
        FileReader fr = new FileReader("C:\\tmp\\" + nomeArquivo);
        BufferedReader reader = new BufferedReader(fr);
        return reader;
    }

    private static DadosRoteiro buildDadosRoteiro(final String[] arr) {
        DadosRoteiro roteiro = new DadosRoteiro();
        roteiro.setOrigem(arr[0]);
        roteiro.setDestino(arr[1]);
        roteiro.setValor(Integer.valueOf(arr[2]));

        //System.out.println(roteiro.toString());
        return roteiro;
    }

    @Override
    public String consultarMelhorRota(final String roteiro) {
        List<DadosRoteiro> roteiros = darCargaDados();
        String[] strs = roteiro.split("-");
        String origem = strs[0];
        String destino = strs[1];

        Map<Integer, Map<Integer, String>> mapResposta = new HashMap<Integer, Map<Integer, String>>();
        Integer valorAuxiliar = 0;
        String roteiroCompleto = null;

        Map<Integer, Map<Integer, String>> roteirosMatch = getDadosRoteiros(roteiros, origem,
                destino, mapResposta, valorAuxiliar, roteiroCompleto);


        if(CollectionUtils.isEmpty(roteirosMatch)){
            return null;
        }

        Map<Integer, String> mapaInterno = new HashMap<>();
        for (Map.Entry<Integer, Map<Integer, String>> map : roteirosMatch.entrySet()) {
            for (Map.Entry<Integer, String> maps : map.getValue().entrySet()){
                if(maps.getValue().contains(destino)){
                    mapaInterno.put(maps.getKey(), maps.getValue());
                }
            }
        }

        if(mapaInterno.isEmpty()){
            return null;
        }

        OptionalInt menorValor = mapaInterno.entrySet().stream().mapToInt(m -> m.getKey()).min();
        Optional<String> resposta = mapaInterno.entrySet().stream()
                .filter(m -> m.getKey() == menorValor.getAsInt())
                .map(r -> {
                    String res = r.getValue() + " > $" + r.getKey();
                    return res;
                })
                .findFirst();

        return resposta.orElse(null);
    }

    private Map<Integer, Map<Integer, String>> getDadosRoteiros(List<DadosRoteiro> roteiros,
                                                                String origem,
                                                                String destino,
                                                                Map<Integer, Map<Integer, String>> mapaRoteiros,
                                                                Integer valorAuxiliar,
                                                                String roteiroCompleto) {

        Map<Integer, String> map = new HashMap<Integer, String>();
        String roteiroCompletoAux = roteiroCompleto;
        boolean reiniciou = false;
        DadosRota dados = null;

        int i = 0;

        List<DadosRoteiro> roteirosMatch = roteiros.stream()
                                                .filter(r -> r.getOrigem().equals(origem))
                                                .collect(Collectors.toList());
        for(DadosRoteiro rot : roteirosMatch){
            if(mapaRoteiros.isEmpty() || reiniciou){
                dados = new DadosRota();
                roteiroCompleto = rot.getOrigem() + " - " + rot.getDestino();
            }else{
                roteiroCompleto = roteiroCompleto + " - " + rot.getDestino();
            }

            valorAuxiliar += rot.getValor();

            if(!destino.equals(rot.getDestino())){
                dados = getRoteirosFilhos(roteiros, rot.getDestino(), destino, valorAuxiliar, roteiroCompleto);
            }

            map.put(Objects.nonNull(dados.getValor()) ? dados.getValor() : valorAuxiliar,
                    Objects.nonNull(dados.getRoteiroCompleto()) ? dados.getRoteiroCompleto() : roteiroCompleto);
            mapaRoteiros.put(i, map);

            valorAuxiliar = 0;
            reiniciou = true;
            roteiroCompleto = null;
            map = new HashMap<Integer, String>();
            i++;
        }

        return mapaRoteiros;
    }

    private DadosRota getRoteirosFilhos(List<DadosRoteiro> roteiros,
                                    String origem,
                                    String destino,
                                    Integer valorAuxiliar,
                                    String roteiroCompleto) {

        DadosRota dados = new DadosRota();

        List<DadosRoteiro> roteirosMatch = roteiros.stream()
                                                    .filter(r -> r.getOrigem().equals(origem))
                                                    .collect(Collectors.toList());
        for(DadosRoteiro rot : roteirosMatch){
            roteiroCompleto = roteiroCompleto + " - " + rot.getDestino();
            valorAuxiliar+= rot.getValor();
            if(destino.equals(rot.getDestino())){
                break;
            }
            dados = getRoteirosFilhos(roteiros, rot.getDestino(), destino, valorAuxiliar, roteiroCompleto);
        }

        dados.setValor(dados.getValor() != null ? dados.getValor() : valorAuxiliar);
        dados.setRoteiroCompleto(dados.getRoteiroCompleto() != null ? dados.getRoteiroCompleto() : roteiroCompleto);

        return dados;
    }

    @Getter
    @Setter
    class DadosRota {
        private String roteiroCompleto;
        private Integer valor;
    }
}
