package com.desafio.bex.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DadosRoteiroResponse {

    private String mensagem;
}
