package com.desafio.bex.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DadosRoteiroRequest {

    @JsonProperty("data")
    private List<DadoRoteiroRequest> roteiros;

}
