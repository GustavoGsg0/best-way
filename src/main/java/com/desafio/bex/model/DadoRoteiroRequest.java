package com.desafio.bex.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DadoRoteiroRequest {

    @JsonProperty("aeroporto_origem")
    private String origem;

    @JsonProperty("aeroporto_destino")
    private String destino;

    @JsonProperty("valor_passagem")
    private Integer valor;

}
