package com.desafio.bex.utils;

import com.desafio.bex.model.DadosRoteiroRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class ObjectUtils {

    private static DadosRoteiroRequest obj;

    public static String convertObjetoRequestToJson(Object objeto) throws JsonProcessingException {

        if(objeto instanceof DadosRoteiroRequest){
            obj = (DadosRoteiroRequest) objeto;
        }
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String objJson= ow.writeValueAsString(obj);

        return objJson;
    }
}
