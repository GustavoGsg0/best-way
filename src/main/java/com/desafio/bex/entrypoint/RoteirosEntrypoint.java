package com.desafio.bex.entrypoint;

import com.desafio.bex.model.DadosRoteiroRequest;
import com.desafio.bex.model.DadosRoteiroResponse;
import com.desafio.bex.useCases.RoteirosUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/roteiros")
public class RoteirosEntrypoint {

    @Autowired
    private RoteirosUseCase service;

    @PostMapping
    public ResponseEntity<DadosRoteiroResponse> inserirRotas(@RequestBody DadosRoteiroRequest roteiros) {
        service.insereRoteiros(roteiros);
        return ResponseEntity.ok(DadosRoteiroResponse.builder()
                                                    .mensagem("Roteiros inseridos com sucesso")
                                                    .build());
    }

    @GetMapping
    public ResponseEntity<String> consultarMelhorRota(@RequestParam String roteiro) {
        String resposta = service.consultarMelhorRota(roteiro);
        HttpStatus http = HttpStatus.OK;
        if(resposta == null){
            http = HttpStatus.NOT_FOUND;
            resposta = "Rota não encontrada";
        }
        return ResponseEntity.status(http).body(resposta);
    }
}
