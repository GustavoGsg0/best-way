package com.desafio.bex.utils;

import com.desafio.bex.model.DadoRoteiroRequest;
import com.desafio.bex.model.DadosRoteiroRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ObjectUtilsTest {

    @Test
    public void convertObjetoRequestToJson() throws JsonProcessingException {
        DadosRoteiroRequest request = new DadosRoteiroRequest();
        List<DadoRoteiroRequest> dados = new ArrayList<>();
        dados.add(DadoRoteiroRequest.builder()
                .origem("VCP")
                .destino("GRU")
                .valor(10)
                .build());
        request.setRoteiros(dados);

        String json = ObjectUtils.convertObjetoRequestToJson(request);

        Assert.assertNotNull(json);
        Assert.assertTrue(json.contains("data"));
        Assert.assertTrue(json.contains("aeroporto_origem"));
        Assert.assertTrue(json.contains("aeroporto_destino"));
        Assert.assertTrue(json.contains("valor_passagem"));
    }
}
