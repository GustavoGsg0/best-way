package com.desafio.bex.entrypoint;

import com.desafio.bex.model.DadoRoteiroRequest;
import com.desafio.bex.model.DadosRoteiroRequest;
import com.desafio.bex.useCases.RoteirosUseCase;
import com.desafio.bex.utils.ObjectUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class RoteirosEntrypointTest {

    @InjectMocks
    private RoteirosEntrypoint entrypoint;

    @MockBean
    private RoteirosUseCase service;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void insereRotas() throws Exception {
        //Dado que eu tenha uma rota para inserir
        DadosRoteiroRequest request = new DadosRoteiroRequest();
        List<DadoRoteiroRequest> dados = new ArrayList<>();
        dados.add(DadoRoteiroRequest.builder()
                .origem("VCP")
                .destino("GRU")
                .valor(10)
                .build());
        request.setRoteiros(dados);

        //Quando eu chamar a api de POST para inserção
        mockMvc.perform(
                post("/v1/roteiros")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(ObjectUtils.convertObjetoRequestToJson(request))
                        .characterEncoding("utf-8"))

        //Então espero que os dados sejam inseridos
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.mensagem").exists())
        .andExpect(jsonPath("$.mensagem").value("Roteiros inseridos com sucesso"))
        .andDo(print());
    }

    @Test
    public void consultaMelhorRotaSucesso() throws Exception {
        String rotaMaisBarata= "VCP - BRC - CDG - ORL - SCL - GRU > $50";
        //Dado que eu tenha uma rota existente para consultar
        String rotaSolicitada = "VCP-GRU";

        //Quando eu chamar a api de GET para consultar
        when(service.consultarMelhorRota(eq(rotaSolicitada))).thenReturn(rotaMaisBarata);
        mockMvc.perform(
                get("/v1/roteiros")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .param("roteiro", rotaSolicitada))

        //Então espero que a consulta ocorra com sucesso e retorne a mensagem esperada
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").value(rotaMaisBarata))
        .andDo(print());

        //E que o serviço foi acionado
        verify(service, times(1)).consultarMelhorRota(eq(rotaSolicitada));
    }

    @Test
    public void consultaRotaInexistente() throws Exception {
        //Dado que eu tenha uma rota inexistente para consultar
        String rotaSolicitada = "VCP-GRU";

        //Quando eu chamar a api de GET para consultar
        when(service.consultarMelhorRota(eq(rotaSolicitada))).thenReturn(null);
        mockMvc.perform(
                get("/v1/roteiros")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .param("roteiro", rotaSolicitada))

        //Então espero que dê 404 e mensagem de rota não encontrada
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$").value("Rota não encontrada"))
        .andDo(print());

        //E que o serviço foi acionado
        verify(service, times(1)).consultarMelhorRota(eq(rotaSolicitada));
    }
}
