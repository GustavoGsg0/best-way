package com.desafio.bex.factory;

import com.desafio.bex.model.DadosRoteiro;

import java.util.ArrayList;
import java.util.List;

public class DadosRoteiroFactory {

    public static List<DadosRoteiro> buildDadosRoteirosDefault() {
        List<DadosRoteiro> dados = new ArrayList<>();
        dados.add(buildDadosRoteiro("GRU", "BRC", 10));
        dados.add(buildDadosRoteiro("BRC", "SCL", 5));
        dados.add(buildDadosRoteiro("GRU", "CDG", 75));
        dados.add(buildDadosRoteiro("GRU", "SCL", 20));
        dados.add(buildDadosRoteiro("GRU", "ORL", 56));
        dados.add(buildDadosRoteiro("ORL", "CDG", 5));
        dados.add(buildDadosRoteiro("SCL", "ORL", 20));
        return dados;
    }

    public static DadosRoteiro buildDadosRoteiro(final String origem, final String destino, final Integer valor) {
        DadosRoteiro roteiro = new DadosRoteiro();
        roteiro.setOrigem(origem);
        roteiro.setDestino(destino);
        roteiro.setValor(valor);
        return roteiro;
    }
}
