package com.desafio.bex.provider;

import com.desafio.bex.factory.DadosRoteiroFactory;
import com.desafio.bex.model.DadosRoteiro;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RoteirosProviderTest {

    @Spy
    private RoteirosProvider provider;

    @Test
    public void consultaRoteiroSimples(){
        //Dado que eu tenha dados dessa consulta no arquivo
        List<DadosRoteiro> dados = DadosRoteiroFactory.buildDadosRoteirosDefault();

        when(provider.darCargaDados()).thenReturn(dados);

        //Quando eu consultar a melhor rota
        String melhorRota = provider.consultarMelhorRota("ORL-CDG");

        //Então espero que a rota seja calculada e formatada corretamente
        Assert.assertNotNull(melhorRota);
        Assert.assertEquals("ORL - CDG > $5", melhorRota);
    }

    @Test
    public void consultaRoteiroEnunciado(){
        //Dado que eu tenha dados dessa consulta no arquivo
        List<DadosRoteiro> dados = DadosRoteiroFactory.buildDadosRoteirosDefault();

        when(provider.darCargaDados()).thenReturn(dados);

        //Quando eu consultar a melhor rota
        String melhorRota = provider.consultarMelhorRota("GRU-CDG");

        //Então espero que a rota seja calculada e formatada corretamente
        Assert.assertNotNull(melhorRota);
        Assert.assertEquals("GRU - BRC - SCL - ORL - CDG > $40", melhorRota);
    }

}
